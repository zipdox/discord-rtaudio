const WebSocket = require('ws');

class VoiceGateway {
    constructor(endpoint, token, session_id, user_id, server_id){
        this.socket = new WebSocket('wss://' + endpoint + '?v=4');
        this.token = token;
        this.sessionID = session_id;
        this.userID = user_id;
        this.serverID = server_id;
        this.socket.on('message', (message)=>{
            this._onMessage(message)
        });
        this.socket.on('close', (code)=>{
            if(typeof this._onClose === 'function') this._onClose(code);
        });
        this.heartbeat;
    }

    _sendPacket(packet){
        this.socket.send(JSON.stringify(packet));
    }

    _onMessage(message){
        let packet = JSON.parse(message);

        if(packet.s !== null) this.seq = packet.s;

        switch(packet.op){
            case 2:
                if(typeof this._ready === 'function') this._ready(packet.d);
                break;
            case 4:
                if(typeof this._sessionDescription === 'function') this._sessionDescription(packet.d);
                break;
            case 5:
                if(typeof this._speaking === 'function') this._speaking(packet.d);
                break;
            case 8:
                const that = this;
                this.heartbeat = setInterval(function(){
                    that._sendPacket({
                        op: 3,
                        d: Math.floor(2**48*Math.random())
                    });
                }, packet.d.heartbeat_interval);
                this._sendPacket({
                    op: 0,
                    d: {
                        server_id: this.serverID, // If a private call then it should be the text channel ID
                        user_id: this.userID,
                        session_id: this.sessionID,
                        token: this.token
                    }
                });
                break;
            case 9:
                // do something with this in the future
                break;
            case 13:
                if(typeof this._clientDisconnect === 'function') this._clientDisconnect(packet.d);
                break;
            case 14:
                // console.log('Wtf, undocumented op code???');
                // console.log(packet);
                break;
            // send only opcodes
            case 0:
            case 1:
            case 3:
            case 7:
            // we can ignore Heartbeat ACK
            case 6:
            default:
                break;
        }
    }

    onClose(callback){
        if(typeof callback !== 'function') throw 'Callback is not a function';
        this._onClose = callback;
    }

    onReady(callback){
        if(typeof callback !== 'function') throw 'Callback is not a function';
        this._ready = callback;
    }

    onSessionDescription(callback){
        if(typeof callback !== 'function') throw 'Callback is not a function';
        this._sessionDescription = callback;
    }

    onSpeaking(callback){
        if(typeof callback !== 'function') throw 'Callback is not a function';
        this._speaking = callback;
    }

    onClientDisconnect(callback){
        if(typeof callback !== 'function') throw 'Callback is not a function';
        this._clientDisconnect = callback;
    }

    selectProtocol(protocol){
        this._sendPacket({
            op: 1,
            d: protocol
        });
    }

    speaking(speak){
        this._sendPacket({
            op: 5,
            d: speak
        });
    }

    close(){
        this.socket.close();
    }
}

module.exports = VoiceGateway;
