const WebSocket = require('ws');

class Gateway {
    constructor(){
        this.callbacks = {};
        this.heartbeat;
        this.seq = null;
    }

    _sendPacket(packet){
        this.socket.send(JSON.stringify(packet));
    }

    _onMessage(message){
        let packet = JSON.parse(message);

        if(packet.s !== null) this.seq = packet.s;

        switch(packet.op){
            case 0:
                if(packet.t === 'READY'){
                    this.session_id = packet.d.session_id;
                    this.user_id = packet.d.user.id;
                }
                if(typeof this.callbacks[packet.t] === 'function') this.callbacks[packet.t](packet.d);
                break;
            case 7:
                this._sendPacket({
                    op: 6,
                    d: {
                      token: this.identity.token,
                      session_id: this.session_id,
                      seq: this.seq
                    }
                  });
                break;
            case 9:
                this._sendPacket({
                    op: 2,
                    d: this.identity
                });
                break;
            case 10:
                clearInterval(this.heartbeat);
                const that = this;
                this.heartbeat = setInterval(()=>{
                    that._sendPacket({
                        op: 1,
                        d: that.seq
                    });
                }, packet.d.heartbeat_interval);
                this._sendPacket({
                    op: 2,
                    d: this.identity
                });
                break;
            // we can ignore received Heartbeats
            case 1:
            // send only opcodes
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 8:
            // we can ignore Heartbeat ACK
            case 11:
            default:
                break;
        }
    }

    _events = [
        'READY',
        'READY_SUPPLEMENTAL',
        'RESUMED',
        'GUILD_CREATE',
        'GUILD_DELETE',
        'GUILD_UPDATE',
        'INVITE_CREATE',
        'INVITE_DELETE',
        'GUILD_MEMBER_ADD',
        'GUILD_MEMBER_REMOVE',
        'GUILD_MEMBER_UPDATE',
        'GUILD_MEMBERS_CHUNK',
        'GUILD_ROLE_CREATE',
        'GUILD_ROLE_DELETE',
        'GUILD_ROLE_UPDATE',
        'GUILD_BAN_ADD',
        'GUILD_BAN_REMOVE',
        'GUILD_EMOJIS_UPDATE',
        'GUILD_INTEGRATIONS_UPDATE',
        'CHANNEL_CREATE',
        'CHANNEL_DELETE',
        'CHANNEL_UPDATE',
        'CHANNEL_PINS_UPDATE',
        'MESSAGE_CREATE',
        'MESSAGE_DELETE',
        'MESSAGE_UPDATE',
        'MESSAGE_DELETE_BULK',
        'MESSAGE_REACTION_ADD',
        'MESSAGE_REACTION_REMOVE',
        'MESSAGE_REACTION_REMOVE_ALL',
        'MESSAGE_REACTION_REMOVE_EMOJI',
        'USER_UPDATE',
        'PRESENCE_UPDATE',
        'TYPING_START',
        'VOICE_STATE_UPDATE',
        'VOICE_SERVER_UPDATE',
        'WEBHOOKS_UPDATE'
    ]

    identify(identity){
        this.socket = new WebSocket('wss://gateway.discord.gg/?encoding=json&v=8');
        this.identity = identity;
        this.socket.on('message', (message)=>{
            this._onMessage(message);
        });
    }

    onEvent(event, callback){
        if(typeof callback !== 'function') throw 'Callback is not a function';
        if(!this._events.includes(event)) throw 'Invalid event';
        this.callbacks[event] = callback;
    }

    presenceUpdate(data){
        this._sendPacket({
            op: 3,
            d: data
        });
    }

    voiceStateUpdate(data){
        this._sendPacket({
            op: 4,
            d: data
        });
    }

    requestGuildMembers(data){
        this._sendPacket({
            op: 8,
            d: data
        });
    }

    close(){
        this.socket.close();
    }
}

module.exports = Gateway;
