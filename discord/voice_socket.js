const {createSocket} = require('dgram');
const {isIPv4, isIPv6} = require('net');

class VoiceSocket{
    constructor(ip, port, ssrc){
        this.protocol = isIPv4(ip) ? 'udp4' : isIPv6(ip) ? 'udp6' : null;
        this.socket = createSocket(this.protocol);
        this.ip = ip;
        this.port = port;
        this.ssrc = ssrc;
    }

    discoverIP(){
        return new Promise((resolve) => {
            let keepCallback;
            const socket = this.socket
            function ipCallback(buffer){
                const publicIP = buffer.slice(8, buffer.indexOf(0, 8)).toString();
                socket.off('message', ipCallback);
                if(keepCallback !== undefined) socket.on('message', keepCallback);
                resolve(publicIP);
            }
            if(typeof this._onMessage === 'function'){
                keepCallback = this._onMessage;
                this.socket.off('message', this._onMessage);
            }
            this.socket.on('message', ipCallback);
            const discoveryBuffer = Buffer.alloc(74);
            discoveryBuffer.writeUInt16BE(1, 0);
            discoveryBuffer.writeUInt16BE(70, 2);
            discoveryBuffer.writeUInt32BE(this.ssrc, 4);
            this.socket.send(discoveryBuffer, this.port, this.ip);
        });
    }

    onMessage(callback){
        if(typeof callback !== 'function') throw 'Callback is not a function';
        this._onMessage = callback;
    }
    send(buffer){
        this.socket.send(buffer, this.port, this.ip);
    }
}

module.exports = VoiceSocket;