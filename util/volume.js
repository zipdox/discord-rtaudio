const power = 2;
const maxVol = 100**power;

module.exports = function(volume){
    return volume ** power / maxVol;
}