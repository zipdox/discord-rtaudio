function cacheUsers(ready){
    const userCache = {};
    for(let guild of ready.guilds){
        for(let member of guild.members){
            userCache[member.id] = member;
        }
    }
    return userCache;
}

module.exports = cacheUsers;