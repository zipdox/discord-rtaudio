const sodium = require('sodium');

const nonce = Buffer.alloc(24);

function decrypt(buffer, secretKey) {
    buffer.copy(nonce, 0, 0, 12);

    const decrypted = sodium.api.crypto_secretbox_open_easy(buffer.slice(12), nonce, secretKey);
    if (!decrypted) return;
    return Buffer.from(decrypted);
}

function parsePacket(buffer, secretKey) {
    let packet = decrypt(buffer, secretKey);
    if (!packet) return;

    // Strip RTP Header Extensions (one-byte only)
    if (packet[0] === 0xBE && packet[1] === 0xDE && packet.length > 4) {
        const headerExtensionLength = packet.readUInt16BE(2);
        let offset = 4;
        for (let i = 0; i < headerExtensionLength; i++) {
            const byte = packet[offset];
            offset++;
            if (byte === 0) continue;
            offset += 1 + (byte >> 4);
        }
        // Skip over undocumented Discord byte (if present)
        const byte = packet.readUInt8(offset);
        if (byte === 0x00 || byte === 0x02) offset++;

        packet = packet.slice(offset);
    }

    return packet;
}

function voicePacket(msg, secretKey){
    if (msg.length <= 8) return;
    const ssrc = msg.readUInt32BE(8);
    const packet = parsePacket(msg, secretKey);
    if(packet == undefined) return
    return {ssrc, data: packet};
}

module.exports = voicePacket;