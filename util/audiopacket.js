const sodium = require('sodium');

const nonce = Buffer.alloc(24);

function createAudioPacket(opusPacket, sequence, timestamp, ssrc, secretKey) {
    const packetBuffer = Buffer.alloc(12);
    packetBuffer[0] = 0x80;
    packetBuffer[1] = 0x78;

    packetBuffer.writeUIntBE(sequence, 2, 2);
    packetBuffer.writeUIntBE(timestamp, 4, 4);
    packetBuffer.writeUIntBE(ssrc, 8, 4);

    packetBuffer.copy(nonce, 0, 0, 12);
    return Buffer.concat([packetBuffer, sodium.api.crypto_secretbox_easy(opusPacket, nonce, secretKey)]);
}

module.exports = createAudioPacket;