module.exports = function(gi, Gtk){
    const GdkPixbuf = gi.require('GdkPixbuf');
    const GLib = gi.require('GLib', '2.0');
    return function(image, width, height){
        if(image){
            let loader;
            if(image.type == 'png') loader = new GdkPixbuf.PixbufLoader('png');
            if(image.type == 'gif') loader = new GdkPixbuf.PixbufLoader('gif');
            loader.setSize(width, height);
            loader.write(image.file);
            if(image.type == 'png') return Gtk.Image.newFromPixbuf(loader.getPixbuf());
            if(image.type == 'gif') return Gtk.Image.newFromAnimation(loader.getAnimation());
            return;
        }else{
            return Gtk.Image.newFromPixbuf(new GdkPixbuf.Pixbuf.newFromBytes(new GLib.Bytes(Buffer.alloc(width*height*4)), 0, true, 8, width, height, height*4));
        }
    }
}