function mix(buffers, bufsize){
    const mixed = Buffer.alloc(bufsize*4); // 2 channels and 2 bytes per sample
    for(let i = 0; i < bufsize*4; i+=2){ //2 bytes per sample
        let sample = 0;
        for(let buffer of buffers){
            sample += buffer.readInt16LE(i);
        }
        if(sample < -32768) sample = -32768;
        if(sample > 32767) sample = 32767;
        mixed.writeInt16LE(sample, i);
    }
    return mixed;
}

module.exports = mix;