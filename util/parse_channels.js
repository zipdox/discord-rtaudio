const fetch = require('node-fetch');

module.exports = async function(ready, callback, finished, respondChannels){
    const directs = [];
    for(let private_channel of ready.private_channels){
        switch(private_channel.type){
            case 1:
                let user = {type: 1, name: private_channel.recipients[0].username, id: private_channel.id, lastmsg: private_channel.last_message_id};
                if(private_channel.recipients[0].avatar) if(private_channel.recipients[0].avatar.startsWith('a_')){
                    user.iconurl = `https://cdn.discordapp.com/avatars/${private_channel.recipients[0].id}/${private_channel.recipients[0].avatar}.gif`;
                }else{
                    user.iconurl =`https://cdn.discordapp.com/avatars/${private_channel.recipients[0].id}/${private_channel.recipients[0].avatar}.png`;
                }
                directs.push(user);
                break;
            case 3:
                let group = {id: private_channel.id, lastmsg: private_channel.last_message_id};
                if(private_channel.name){
                    group.name = private_channel.name;
                }else{
                    if(private_channel.recipients[0] === undefined) continue;
                    group.name = private_channel.recipients[0].username;
                    for(let i = 1; i < private_channel.recipients.length; i++) group.name += ', ' + private_channel.recipients[i].username;
                }
                if(private_channel.icon) if(private_channel.icon.startsWith('a_')){
                    group.iconurl = `https://cdn.discordapp.com/channel-icons/${private_channel.id}/${private_channel.icon}.gif`;
                }else{
                    group.iconurl = `https://cdn.discordapp.com/channel-icons/${private_channel.id}/${private_channel.icon}.png`;
                }
                directs.push(group);
                break;
            default:
                console.error('Unknown private channel type', private_channel);
                break;
        }
    }
    directs.sort((a, b) => Number(b.lastmsg) - Number(a.lastmsg));

    const guilds = [];
    for(let guild of ready.guilds){
        let guildObject = {name: guild.name, id: guild.id, channels: []};
        if(guild.icon) if(guild.icon.startsWith('a_')){
            guildObject.iconurl = `https://cdn.discordapp.com/icons/${guild.id}/${guild.icon}.gif`;
        }else{
            guildObject.iconurl = `https://cdn.discordapp.com/icons/${guild.id}/${guild.icon}.png`;
        }
        for(let channel of guild.channels){
            if(channel.type !== 2) continue;
            guildObject.channels.push({name: channel.name, id: channel.id});
        }
        guilds.push(guildObject);
    }
    
    let sortedGuilds = [];
    if(ready.user_settings.guild_positions.length > 0){
        for(let guildID of ready.user_settings.guild_positions){
            sortedGuilds.push(guilds.find(obj => {
                return obj.id == guildID;
            }));
        }
    }else{
        sortedGuilds = guilds;
    }

    respondChannels({directs, guilds});

    for(let direct of directs){
        if(direct.iconurl){
            direct.icon = {};
            if(direct.iconurl.endsWith('.png')) direct.icon.type = 'png';
            if(direct.iconurl.endsWith('.gif')) direct.icon.type = 'gif';
            direct.icon.file = await (await fetch(direct.iconurl)).buffer();
        }
        callback({type: 'direct', direct});
    }

    for(let guild of sortedGuilds){
        if(guild.iconurl){
            guild.icon = {};
            if(guild.iconurl.endsWith('.png')) guild.icon.type = 'png';
            if(guild.iconurl.endsWith('.gif')) guild.icon.type = 'gif';
            guild.icon.file = await (await fetch(guild.iconurl)).buffer();
        }
        callback({type: 'guild', guild});
    }
    finished();
}