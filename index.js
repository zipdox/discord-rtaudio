const {Worker} = require('worker_threads');
const gi = require('node-gtk');
const Gtk = gi.require('Gtk', '3.0');
const volumeScale = require('./util/volume.js');
const imageLoader = require('./util/image_loader.js')(gi, Gtk);

const worker = new Worker('./worker.js');

gi.startLoop();
Gtk.init();

const win = new Gtk.Window();
win.setTitle('Discord RtAudio');
win.setDefaultSize(640, 640);

const splitBox = new Gtk.HBox({orientation: Gtk.Orientation.HORIZONTAL});

const directsScrollable = new Gtk.ScrolledWindow(null, null);
const directsBox = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL});
directsScrollable.add(directsBox)
splitBox.add(directsScrollable);

const guildsScrollable = new Gtk.ScrolledWindow(null, null);
const guildsBox = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL});
guildsScrollable.add(guildsBox)
splitBox.add(guildsScrollable);

const statusScrollable = new Gtk.ScrolledWindow(null, null);
const statusBox = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL});
statusScrollable.add(statusBox)
splitBox.add(statusScrollable);

win.add(splitBox);

win.on('destroy', () => {
    Gtk.mainQuit();
    worker.postMessage({type: 'exit'});
});

const leaveButton = new Gtk.Button({label: 'Leave'});
leaveButton.on('clicked', ()=>{
    worker.postMessage({type: 'leave'});
});

const muteButton = new Gtk.Button({label: 'Mute'});
let muted = false;
muteButton.on('clicked', ()=>{
    muted = !muted;
    worker.postMessage({type: 'muted', muted});
    if(muted){
        muteButton.setLabel('Unmute');
    }else{
        muteButton.setLabel('Mute');
    }
});
statusBox.add(muteButton);

const channelUsersBox = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL});
statusBox.add(channelUsersBox);
let channelUsers = {};

const status = new Gtk.Label({label: 'Starting...'});
statusBox.packEnd(status, false, false, 4);

worker.on('exit', process.exit);

worker.on('message', message => {
    switch(message.type){
        case 'direct':
            addDirect(message.direct);
            break;
        case 'guild':
            addGuild(message.guild);
            break;
        case 'connected':
            if(leaveButton.getParent() == null) statusBox.packEnd(leaveButton, false, false, 4);
            win.showAll();
            break;
        case 'status':
            status.setLabel(message.status);
            break;
        case 'left':
            if(leaveButton.getParent() != null) statusBox.remove(leaveButton);
            for(let channelUser in channelUsers){
                channelUsersBox.remove(channelUsers[channelUser].box);
            }
            channelUsers = {};
            break;
        case 'speaking':
            if(channelUsers[message.speaking.user.id]) return;
            channelUsers[message.speaking.user.id] = {
                box: new Gtk.Box({orientation: Gtk.Orientation.VERTICAL}),
                name: new Gtk.Label({label: message.speaking.user.username}),
                adjustment: new Gtk.Adjustment()
            }
            channelUsers[message.speaking.user.id].adjustment.setLower(0);
            channelUsers[message.speaking.user.id].adjustment.setUpper(100);
            channelUsers[message.speaking.user.id].adjustment.setValue(100);
            channelUsers[message.speaking.user.id].adjustment.on('value-changed', () => {
                worker.postMessage({type: 'volume', user: message.speaking.user.id, volume: volumeScale(channelUsers[message.speaking.user.id].adjustment.value)});
            });
            channelUsers[message.speaking.user.id].slider = new Gtk.Scale({orientation: Gtk.Orientation.HORIZONTAL, adjustment: channelUsers[message.speaking.user.id].adjustment});
            channelUsers[message.speaking.user.id].box.add(channelUsers[message.speaking.user.id].name);
            channelUsers[message.speaking.user.id].box.add(channelUsers[message.speaking.user.id].slider);
            channelUsersBox.add(channelUsers[message.speaking.user.id].box);
            win.showAll();
            break;
        case 'userdisconnect':
            if(!channelUsers[message.user]) return;
            channelUsersBox.remove(channelUsers[message.user].box);
            delete channelUsers[message.user];
            break;
        default:
            // console.log(message);
            break;
    }
});

function joinChannel(guild, channel){
    worker.postMessage({type: 'connect', channel: {guild, channel}});
}

directsBox.add(new Gtk.Label({label: 'Directs'}));
function addDirect(direct){
    let channelBox = new Gtk.Box({orientation: Gtk.Orientation.HORIZONTAL});

    channelBox.add(imageLoader(direct.icon, 32, 32));
    
    let channelExpander = new Gtk.Expander();
    channelExpander.setLabelWidget(new Gtk.Label({label: direct.name}));

    const channelsChildBox = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL});
    const channelButton = new Gtk.Button({label: 'Join'});
    channelButton.on('clicked', ()=>{
        joinChannel(null, direct.id);
    });
    channelExpander.add(channelsChildBox);
    channelsChildBox.add(channelButton);

    channelBox.add(channelExpander);
    directsBox.add(channelBox);

    win.showAll();
}

guildsBox.add(new Gtk.Label({label: 'Guilds'}));
function addGuild(guild){
    let guildBox = new Gtk.Box({orientation: Gtk.Orientation.HORIZONTAL});

    guildBox.add(imageLoader(guild.icon, 32, 32));

    let guildExpander = new Gtk.Expander();
    guildExpander.setLabelWidget(new Gtk.Label({label: guild.name}));

    guildChannelsBox = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL});
    guildExpander.add(guildChannelsBox);
    for(let channel of guild.channels){
        const channelExpander = new Gtk.Expander();
        channelExpander.setLabelWidget(new Gtk.Label({label: channel.name}));
        channelExpander.setMarginLeft(16);

        const channelsChildBox = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL});
        const channelButton = new Gtk.Button({label: 'Join'});
        channelButton.on('clicked', ()=>{
            joinChannel(guild.id, channel.id);
        });
        channelExpander.add(channelsChildBox);
        channelsChildBox.add(channelButton);

        guildChannelsBox.add(channelExpander);
    }

    guildBox.add(guildExpander);
    guildsBox.add(guildBox);
    win.showAll();
}

win.showAll();
Gtk.main();
