const {createSocket} = require('dgram');
const {isIPv4, isIPv6} = require('net');
const fs = require('fs');

const VoiceGateway = require('./discord/voice_gateway.js');
const Gateway = require('./discord/gateway.js');
const discoveryBuffer = require('./discoverybuffer.js');
const createAudioPacket = require('./audiopacket.js');

const demuxedOpus = fs.readFileSync('test.opus');


const gateway = new Gateway({
    token: '[REDACTED]',
    properties: {
        os: 'Linux',
        browser: 'Firefox'
    }
});

let voiceGateway;
let udpSocket;
let voiceSsrc;
let voicePort;
let voiceIP;
let voiceSessionID;
let voiceKey;
let voiceSeq = 0;

gateway.onEvent('VOICE_SERVER_UPDATE', state => {
    console.log(state);
    voiceGateway = new VoiceGateway(state.endpoint, state.token, gateway.session_id, gateway.user_id, state.channel_id);
    voiceGateway.onReady(ready => {
        console.log(ready);

        udpSocket = createSocket(isIPv4(ready.ip) ? 'udp4' : isIPv6(ready.ip) ? 'udp6' : null);
        udpSocket.on('message', buffer => {
            const publicIP = buffer.slice(8, buffer.indexOf(0, 8)).toString();
            console.log(publicIP);
            voiceGateway.onSessionDescription(session => {
                console.log(session);
                voiceSessionID = session.media_session_id;
                voiceKey = new Uint8Array(session.secret_key);
            });
            voiceGateway.selectProtocol({
                protocol: 'udp',
                data: {
                    address: publicIP,
                    port: ready.port,
                    mode: 'xsalsa20_poly1305'
                }
            });
        });
        udpSocket.send(discoveryBuffer(ready.ssrc), ready.port, ready.ip);
        voiceSsrc = ready.ssrc;
        voicePort = ready.port;
        voiceIP = ready.ip;
    });
    // voiceGateway.onSpeaking(update => {
    //     console.log('Speaking', update);
    // });
    // voiceGateway.onClientDisconnect(update => {
    //     console.log('Disconnect', update);
    // });
})

gateway.onEvent('MESSAGE_CREATE', msg => {
    if(msg.content === '!joinme'){
        gateway.voiceStateUpdate({
            guild_id: null,
            channel_id: msg.channel_id,
            self_mute: false,
            self_deaf: false
        });
    }
    if(msg.content === '!leaveme'){
        gateway.voiceStateUpdate({
            guild_id: null,
            channel_id: null,
            self_mute: false,
            self_deaf: false
        });
        voiceGateway.close();
    }
    if(msg.content === '!playtune'){
        const audioPacket = createAudioPacket(demuxedOpus, voiceSeq, 0, voiceSsrc, voiceKey, voiceKey);
        console.log(audioPacket);
        voiceGateway.speaking({
            speaking: 1,
            delay: 0,
            ssrc: voiceSsrc
        });
        udpSocket.send(audioPacket, voicePort, voiceIP);
    }
});
