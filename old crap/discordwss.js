const WebSocket = require('ws');
const {createSocket} = require('dgram');
const {isIPv4, isIPv6} = require('net');
const fs = require('fs');
const sodium = require('sodium');
const audioPacket = require('./audiopacket.js');

const creds = require('./testshit.json');

const opusTest = fs.readFileSync('test.opus');

let globalData = {
    private_channels: [],
    guilds: []
}

let heartBeat;
let seqNumber = null;
let user;
let sessionID;

const socket = new WebSocket('wss://gateway.discord.gg/?encoding=json&v=8');
let voiceSocket;
let voiceUDPSocket;
let connData;
let voiceKeepAlive;
let keepAliveBuffer = Buffer.alloc(8);
let publicIP;

const nonce = Buffer.alloc(24);

function createAudioPacket(opusPacket, connectionData) {
    const packetBuffer = Buffer.alloc(12);
    packetBuffer[0] = 0x80;
    packetBuffer[1] = 0x78;

    const { sequence, timestamp, ssrc } = connectionData;

    packetBuffer.writeUIntBE(sequence, 2, 2);
    packetBuffer.writeUIntBE(timestamp, 4, 4);
    packetBuffer.writeUIntBE(ssrc, 8, 4);

    packetBuffer.copy(nonce, 0, 0, 12);
    return Buffer.concat([packetBuffer, encryptOpusPacket(opusPacket, connectionData)]);
}

function encryptOpusPacket(opusPacket, connectionData){
    const { secretKey, encryptionMode } = connectionData;

    return sodium.api.crypto_secretbox_easy(opusPacket, nonce, secretKey);
}

function handleUDPMessage(buffer){
    console.log(buffer);
}

function createVoiceUDPSOcket(params){
    const protocol = isIPv4(params.ip) ? 'udp4' : isIPv6(params.ip) ? 'udp6' : null;
    voiceUDPSocket = createSocket(protocol);
    voiceUDPSocket.on('error', error => console.error(error));
    voiceUDPSocket.on('message', buffer => {
        publicIP = buffer.slice(8, ipBuf.indexOf(0, 8)).toString();
        console.log(publicIP);
        voiceUDPSocket.on('message', handleUDPMessage);
        voiceSocket.send(JSON.stringify({
            op: 1,
            d: {
                protocol: 'udp',
                data: {
                    address: publicIP,
                    port: params.port,
                    mode: 'xsalsa20_poly1305'
                }
            }
        }));
    });
    // voiceKeepAlive = setInterval(function(){

    // }, 5000);

    const discoveryBuffer = Buffer.alloc(74);
    discoveryBuffer.writeUInt16BE(1, 0);
    discoveryBuffer.writeUInt16BE(70, 2);
    discoveryBuffer.writeUInt32BE(params.ssrc, 4);
    voiceUDPSocket.send(discoveryBuffer, params.port, params.ip);
}

function createVoiceSocket(voiceCreds){
    voiceSocket = new WebSocket('wss://' + voiceCreds.endpoint + '?v=4');
    let voiceHeartBeat;
    voiceSocket.on('message', msg => {
        let parsed = JSON.parse(msg);
        switch(parsed.op){
            case 2:
                createVoiceUDPSOcket(parsed.d);
                connData = parsed.d;
                break;
            case 4:
                voiceSocket.send(JSON.stringify({
                    op: 5,
                    d: {
                        speaking: 5,
                        delay: 0,
                        ssrc: connData.ssrc
                    }
                }));
                connectionData = {
                    encryptionMode: parsed.d.mode,
                    secretKey: new Uint8Array(parsed.d.secret_key),
                    sequence: randomNBit(16),
                    timestamp: randomNBit(32),
                    ssrc: connData.ssrc,
                    nonce: 0,
                    nonceBuffer: Buffer.alloc(24),
                    speaking: false,
                    packetsPlayed: 0,
                }
                let voicePacket = createAudioPacket(opusTest, connectionData)
                setTimeout(function(){
                    voiceUDPSocket.send(voicePacket, connData.port, connData.ip);
                }, 200);
                
                break;
            case 8:
                voiceHeartBeat = setInterval(function(){
                    voiceSocket.send(JSON.stringify({
                        op: 3,
                        d: Math.floor(2**48*Math.random())
                    }));
                }, parsed.d.heartbeat_interval);
                voiceSocket.send(JSON.stringify({
                    op: 0,
                    d: {
                        server_id: voiceCreds.guild_id !== null ? voiceCreds.guild_id : voiceCreds.channel_id,
                        user_id: user.id,
                        session_id: sessionID,
                        token: voiceCreds.token
                    }
                }));
                break;
            default:
                break;
        }
        console.log(parsed)
    });
}

socket.on('message', msg => {
    let parsed = JSON.parse(msg);
    switch(parsed.op){
        case 0:
            switch(parsed.t){
                case 'READY':
                    if(parsed.d.private_channels) globalData.private_channels = parsed.d.private_channels;
                    if(parsed.d.guilds) globalData.guilds = parsed.d.guilds;
                    user = parsed.d.user;
                    sessionID = parsed.d.session_id;
                    break;
                case 'READY_SUPPLEMENTAL':
                    break;
                case 'MESSAGE_CREATE':
                    // console.log(parsed.d.author.username, parsed.d.content);
                    if(parsed.d.content == '!test') console.log(globalData);
                    if(parsed.d.content == '!doit'){
                        socket.send(JSON.stringify({
                            op: 3,
                            d: {
                                since: 91879201,
                                activities: [{
                                    name: 'Earrape',
                                    type: 0
                                }],
                                status: 'dnd',
                                afk: false
                            }
                        }));
                    }
                    if(parsed.d.content == '!joinme'){
                        socket.send(JSON.stringify({
                            op: 4,
                            d: {
                                guild_id: null,
                                channel_id: parsed.d.channel_id,
                                self_mute: false,
                                self_deaf: false
                            }
                        }));
                    }
                    break;
                case 'VOICE_STATE_UPDATE':
                    break;
                case 'VOICE_SERVER_UPDATE':
                    createVoiceSocket(parsed.d);
                    console.log(parsed);
                    break;
                default:
                    break;
            }
            break;
        case 10:
            heartBeat = setInterval(function(){
                socket.send(JSON.stringify({
                    op: 1,
                    d: seqNumber
                }));
            }, parsed.d.heartbeat_interval);
            socket.send(JSON.stringify({
                op: 2,
                d: {
                    token: 'dont think so m8',
                    capabilities: 61,
                    properties: {
                        os: 'Linux',
                        browser: 'Firefox',
                        device: ''
                    }
                }
            }));
        default:
            break;
    }
    if(parsed.s !== null) seqNumber = parsed.s;
    // console.log(parsed);
});

function randomNBit(n) {
    return Math.floor(Math.random() * 2 ** n);
}