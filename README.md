#  Discord RtAudio
A Discord voice client that uses [RTAudio](https://github.com/thestk/rtaudio) to support high-quality low-latency communication.

# WIP
**This application is a work in progress. It is not stable and is not reliable.**

# Features
- Automatic Discord account detection
- Low latency
- Extensive [audio API](https://almoghamdani.github.io/audify/enums/rtaudioapi.html) support
- Custom sending bitrate
- Stereo
- No pre-processing

# Installation
To install this application you need to have [NodeJS](https://nodejs.org/en/) and NPM installed.

Simply clone/download this repo and run `npm install` in its directory.

## Configuration
Create a copy of `config_example.json` named `config.json` and edit it. Enter the [audio API](https://almoghamdani.github.io/audify/enums/rtaudioapi.html) you wish to use, the name of your audio device, the buffer size you configured the device to use, and the bitrate you wish to send audio at.

If you wish to use different account from the one you're logged into on Discord, simply add the `token` key to the config file, along with the token of the account. If you don't have your token, you can find it by logging into Discord in your browser, opening developer tools, clicking the network tab and putting `/api/` into the filter box, and clicking any of the requests. Click the `Headers` tab and scroll down to request headers. Copy the value of the `authorization` header, that is your token. Don't share your token with anyone!

The app is started by running `npm start`.

Note that this application doesn't have any pre-processing so you will be constantly "speaking" in Discord. You should use your own noise gate if you don't want to be transmitting noise constantly.

## Common issues

### RtAudio: no compiled support for specified API argument!
If you get this error it means that the [audify module](https://github.com/almoghamdani/audify) was compiled without support for the audio API you selected. You will have to compile it yourself until the developer fixes this for the precompiled versions.

You will need to have `cmake` and `make` installed, as well as any development packages for the API you wish to use.
```bash  
cd node_modules/audify
npm install
npm run build-binaries
cd ../..
```
After building the module, RtAudio should work.

### Finding token...
If the status (bottom right) gets stuck on "Finding token..." it probably means that Discord has removed the token from its config files. Closing Discord before starting Discord RtAudio should fix it.

# To do
- Handle being moved to another channel
- Don't wait for users to speak before adding them to the channel preview
- User speaking indicator
- Error handling
- Packaging
- VU meter or input indicator
- Noise gate (maybe)
- Server folders (maybe)
