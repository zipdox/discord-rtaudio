const { RtAudio, RtAudioFormat, RtAudioApi, RtAudioStreamFlags } = require('audify');
const prism = require('prism-media');
const FIFO = require('fifo-buffer');
const {parentPort} = require("worker_threads");
const fetch = require('node-fetch');

process.on('uncaughtException', console.log);

const tokenFinder = require('./util/token_finder.js');
const VoiceGateway = require('./discord/voice_gateway.js');
const Gateway = require('./discord/gateway.js');
const VoiceSocket = require('./discord/voice_socket.js')
const createAudioPacket = require('./util/audiopacket.js');
const voicePacket = require('./util/voice_packet.js');
const parseChannels = require('./util/parse_channels.js');
const mix = require('./util/mix.js');
const {timeIncrement, frameSize} = require('./util/constants.js');

const {bufsize, api, streamFlags, device, bitrate, recbuf, token} = require('./config.json');

const rtAudio = new RtAudio(RtAudioApi[api]);
const devices = rtAudio.getDevices();
let deviceId;
for(let i = 0; i < devices.length; i++){
    if(devices[i].name === device){
        deviceId = i;
        break;
    }
}
if(deviceId == undefined){
    console.log("Couldn't find audio device called", device);
    console.log('Available devices:');
    for(let i = 0; i < devices.length; i++){
        console.log(' - ' + devices[i].name);
    }
    setTimeout(process.exit, 1);
}else{
    rtAudio.openStream(
        {
            deviceId,
            nChannels: 2,
            firstChannel: 0
        },
        {
            deviceId,
            nChannels: 2,
            firstChannel: 0
        },
        RtAudioFormat.RTAUDIO_SINT16,
        48000,
        bufsize,
        "Discord RtAudio",
        null,
        null,
        streamFlags
    );
    rtAudio.start();
}



const gateway = new Gateway();
let channels;
let muted = false;
let connectingChannelID;
let userCache = {};

let voiceGateway;
let voiceSocket;
let publicIP;
let voiceKey;
let encoder;

process.on('uncaughtException', console.log);

let voiceSeq = 0;
let voiceTime = 0;

let userDecoders = {};

if(token){
    parentPort.postMessage({type: 'status', status: 'Logging in...'});
    gateway.identify({
        token,
        properties: {
            os: 'Linux',
            browser: 'Firefox'
        }
    });
}else{
    parentPort.postMessage({type: 'status', status: 'Finding token...'});
    tokenFinder().then(token => {
        parentPort.postMessage({type: 'status', status: 'Logging in...'});
        gateway.identify({
            token,
            properties: {
                os: 'Linux',
                browser: 'Firefox'
            }
        });
    });
}

function attachInput(yes){
    if(encoder == undefined) return;
    if(yes){
        rtAudio.setInputCallback(data => {
            if(!muted){
                encoder.write(data);
            }else{
                encoder.write(Buffer.alloc(bufsize*4));
            }
            rtAudio.write(getPlayBuf(bufsize));
        });
    }else{
        rtAudio.setInputCallback(data => {
            return;
        });
    }
}

function encoderData(data){
    voiceSocket.send(createAudioPacket(data, voiceSeq, voiceTime, voiceSocket.ssrc, voiceKey));
    voiceSeq++;
    if(voiceSeq > 65535) voiceSeq -= 65535;
    voiceTime += timeIncrement;
    if(voiceTime > 4294967295) voiceTime -= 4294967295;
}

function initEncoder(yes){
    if(yes){
        encoder = new prism.opus.Encoder({frameSize: frameSize, channels: 2, rate: 48000});
        encoder.setBitrate(bitrate);
        encoder.on('data', encoderData);
    }else{
        if(encoder == undefined) return;
        encoder.off('data', encoderData);
        delete encoder;
    }
}

function sessionDescription(description){
    voiceKey = new Uint8Array(description.secret_key);

    voiceGateway.speaking({
        speaking: 1,
        delay: 0,
        ssrc: voiceSocket.ssrc
    });
    initEncoder(true);
    attachInput(true);
}

function getPlayBuf(size){
    const bufs = [Buffer.alloc(size*4)];
    for(let user in userDecoders){
        let userBuf = userDecoders[user].buffer.deq(size*4);
        if(userBuf == null) continue;
        bufs.push(userBuf);
    }
    const mixedBuf = mix(bufs, size);
    return mixedBuf;
}

async function userSpeaking(speaking){
    if(userCache[speaking.user_id] != undefined){
        parentPort.postMessage('speaking', {
            user: userCache[speaking.user_id],
            speaking: speaking.speaking
        });
        return;
    }
    const userRequest = await fetch(`https://discord.com/api/v8/users/${speaking.user_id}`, {
        headers: {
            Authorization: gateway.identity.token
        }
    });
    const userResponse = await userRequest.json();
    userCache[userResponse.id] = userResponse;
    parentPort.postMessage({type: 'speaking', speaking: {
        user: userCache[speaking.user_id],
        speaking: speaking.speaking
    }});
}

function userDisconnect(user){
    parentPort.postMessage({type: 'userdisconnect', user: user.user_id});
    for(userDecoder in userDecoders){
        if(userDecoders[userDecoder].id != user.user_id) continue;
        delete userDecoders[userDecoder];
        break;
    }
}

function voiceOnSpeaking(speaking){
    userSpeaking(speaking);
    if(userDecoders[speaking.ssrc]) return;
    userDecoders[speaking.ssrc] = {
        id: speaking.user_id,
        decoder: new prism.opus.Decoder({frameSize: frameSize, channels: 2, rate: 48000}),
        transformer: new prism.VolumeTransformer({ type: 's16le', volume: 1 }),
        buffer: new FIFO(recbuf*4),
        volume: 1
    };
    userDecoders[speaking.ssrc].decoder.pipe(userDecoders[speaking.ssrc].transformer);
    userDecoders[speaking.ssrc].transformer.on('data', data => userDecoders[speaking.ssrc].buffer.enq(data));
}

async function voiceGatewayReady(ready){
    voiceSocket = new VoiceSocket(ready.ip, ready.port, ready.ssrc);
    voiceSocket.onMessage(message => {
        const parsedPacket = voicePacket(message, voiceKey);
        if(parsedPacket == undefined) return;
        if(userDecoders[parsedPacket.ssrc] == undefined) return;
        userDecoders[parsedPacket.ssrc].decoder.write(parsedPacket.data);
    });
    publicIP = await voiceSocket.discoverIP();
    voiceGateway.selectProtocol({
        protocol: 'udp',
        data: {
            address: publicIP,
            port: voiceSocket.port,
            mode: 'xsalsa20_poly1305'
        }
    });
    voiceGateway.onSpeaking(voiceOnSpeaking);
    voiceGateway.onClientDisconnect(userDisconnect);
}

function voiceGatewayClose(){
    attachInput(false);
    initEncoder(false);
    parentPort.postMessage({type: 'status', status: 'Logged in'});
    parentPort.postMessage({type: 'left'});
    if(voiceGateway == undefined) return;
    voiceGateway.close();
    userDecoders = {};
    if(voiceSocket == undefined) return;
    voiceSocket = undefined;
}

function voiceServerUpdate(state){
    let channelName;
    if(state.channel_id){
        channelName = channels.directs.find(obs => {return obs.id === state.channel_id}).name;
    }else{
        const channelGuild = channels.guilds.find(obs => {return obs.id === state.guild_id});
        channelName = channelGuild.channels.find(obs => {return obs.id === connectingChannelID}).name + ' in ' + channelGuild.name;
    }
    parentPort.postMessage({type: 'connected'});
    parentPort.postMessage({type: 'status', status: 'Connected to ' + channelName});
    voiceGateway = new VoiceGateway(state.endpoint, state.token, gateway.session_id, gateway.user_id, state.channel_id ? state.channel_id : state.guild_id);
    voiceGateway.onReady(voiceGatewayReady);
    voiceGateway.onSessionDescription(sessionDescription);
    voiceGateway.onClose(voiceGatewayClose);
}

function onReady(ready){
    parseChannels(ready, (msg)=>{
        parentPort.postMessage(msg);
    },
    ()=>{
        parentPort.postMessage({type: 'status', status: 'Logged in'});
    },
    (fetchedChannels)=>{
        channels = fetchedChannels;
    });
    parentPort.postMessage({type: 'status', status: 'Loading channels...'});
}

gateway.onEvent('VOICE_SERVER_UPDATE', voiceServerUpdate);

gateway.onEvent('READY', onReady);

function leaveChannel(){
    gateway.voiceStateUpdate({
        guild_id: null,
        channel_id: null,
        self_mute: false,
        self_deaf: false
    });
    attachInput(false);
    initEncoder(false);
    parentPort.postMessage({type: 'status', status: 'Logged in'});
    parentPort.postMessage({type: 'left'});
    if(voiceGateway == undefined) return;
    voiceGateway.close();
    userDecoders = {};
    if(voiceSocket == undefined) return;
    voiceSocket = undefined;
}

parentPort.on('message', message =>{
    switch(message.type){
        case 'connect':
            connectingChannelID = message.channel.channel;
            gateway.voiceStateUpdate({
                guild_id: message.channel.guild,
                channel_id: message.channel.channel,
                self_mute: false,
                self_deaf: false
            });
            break;
        case 'leave':
            leaveChannel();
            break;
        case 'muted':
            muted = message.muted;
            break;
        case 'exit':
            leaveChannel();
            gateway.close();
            process.exit();
        case 'volume':
            for(user in userDecoders){
                if(userDecoders[user].id != message.user) continue;
                userDecoders[user].transformer.setVolume(message.volume);
                break;
            }
            break;
        default:
            break;
    }
});
